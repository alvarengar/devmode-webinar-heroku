const express = require("express");
const port = process.env.PORT || 3000;

const app = express();

app.get("/", (req, res) => {
  res.send("Olá coleguinhas do devmode");
});

app.get("/about", (req, res) => {
  res.send("esse sou eu, prazer, olá");
});

app.listen(port, () => {
  console.log(`Tamo de pé na porta ${port} coleguinhas! =D`);
});